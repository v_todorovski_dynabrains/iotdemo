sap.ui.define([
		"dynabrains/iot/demo/controller/BaseController",
		"sap/ui/model/json/JSONModel",
		"sap/ui/core/routing/History",
		"dynabrains/iot/demo/model/formatter",
		"sap/ui/model/Filter",
		"sap/ui/model/FilterOperator"
	], function (BaseController, JSONModel, History, formatter, Filter, FilterOperator) {
		"use strict";

		return BaseController.extend("dynabrains.iot.demo.controller.Worklist", {

			formatter: formatter,

			/* =========================================================== */
			/* lifecycle methods                                           */
			/* =========================================================== */

			/**
			 * Called when the worklist controller is instantiated.
			 * @public
			 */
			onInit : function () {
				var oViewModel,
					iOriginalBusyDelay,
					oTable = this.byId("table");

				// Put down worklist table's original value for busy indicator delay,
				// so it can be restored later on. Busy handling on the table is
				// taken care of by the table itself.
				// iOriginalBusyDelay = oTable.getBusyIndicatorDelay();
				// keeps the search state
				this._oTableSearchState = [];

				// Model used to manipulate control states
				oViewModel = new JSONModel({
					worklistTableTitle : this.getResourceBundle().getText("worklistTableTitle"),
					saveAsTileTitle: this.getResourceBundle().getText("saveAsTileTitle", this.getResourceBundle().getText("worklistViewTitle")),
					shareOnJamTitle: this.getResourceBundle().getText("worklistTitle"),
					shareSendEmailSubject: this.getResourceBundle().getText("shareSendEmailWorklistSubject"),
					shareSendEmailMessage: this.getResourceBundle().getText("shareSendEmailWorklistMessage", [location.href]),
					tableNoDataText : this.getResourceBundle().getText("tableNoDataText"),
					tableBusyDelay : 0
				});
				this.setModel(oViewModel, "worklistView");

				// Make sure, busy indication is showing immediately so there is no
				// break after the busy indication for loading the view's meta data is
				// ended (see promise 'oWhenMetadataIsLoaded' in AppController)
				// oTable.attachEventOnce("updateFinished", function(){
				// 	// Restore original busy indicator delay for worklist's table
				// 	oViewModel.setProperty("/tableBusyDelay", iOriginalBusyDelay);
				// });
				
				this.getView().getAggregation("content")[0].getAggregation("_dynamicPage").getAggregation("content").getAggregation("items")[0].setProperty("colorPalette", ['#d32030', '#27a3dd', '#ffffff']);
				// this.getView().getAggregation("content")[0].getAggregation("_dynamicPage").getAggregation("content").setProperty("colorPalette", ['#d32030', '#27a3dd', '#ffffff']);
				
				this.getData();
				setInterval(this.getData.bind(this), 2000);
			},
			
			counter : 0,
			getData : function () {
				var that = this;
				var url = window.location.protocol + "//" + window.location.host + "/com.sap.iotservices.mms/v1/internal/http/app.svc";
				var oJSONModel = new sap.ui.model.json.JSONModel();
				var oModel = new sap.ui.model.odata.v2.ODataModel(url, { 
					useBatch: false,
					serviceUrlParams : {
						"$orderby" : "G_CREATED desc",
						"$top": "1",
						"$format": "json"
					}
				});
				//////////////////////////////////////////////////////////////////////////////////////////////
				// ///////////////////////////////////////////////////////////////////////////////////////////
				oModel.read("/NEO_9S63NIOFLC8KKFCJ1OUZERRHF.T_IOT_0368B96128897DCFE2E0", {
					success: function(oData, response) {
						oData.results = oData.results.reverse();
						
						oJSONModel.setData(oData);
						that.getOwnerComponent().setModel(oJSONModel, "colorsIoTModel");
						that.setModel(oJSONModel, "colorsIoTModel");
						that.getModel("colorsIoTModel").refresh();
						
						// function compare(a,b) {
						//   if (a.G_CREATED < b.G_CREATED)
						//     return -1;
						//   if (a.G_CREATED > b.G_CREATED)
						//     return 1;
						//   return 0;
						// }
						
						// objs.sort(compare);
						
						var lastValues = oData.results.reduce(function(last, record) {
							if(Number.parseInt(record.C_CHECKED) > last.latestChecked) {
								last.red = Number.parseInt(record.C_RED);
								last.blue = Number.parseInt(record.C_BLUE);
								last.white = Number.parseInt(record.C_WHITE);
								last.latestChecked = Number.parseInt(record.C_CHECKED);
							}
							return last;
						}, { red: 0, blue: 0, white: 0, latestChecked: 0});
						
						
						// oData.results[0].C_RED = (Number.parseFloat(oData.results[0].C_RED) + (that.counter+=12345)).toString();
						
						// var sumValues = oData.results.reduce(function(sum, record) {
						// 	if(record.C_RED !== "0.0" && Number.parseFloat(record.C_RED) > Number.parseInt(sum.redPrev)) {
						// 		if(sum.redPrev === "0.0") {
						// 			sum.red += Number.parseInt(record.C_RED);
						// 		}
						// 		sum.redPrev = record.C_RED;
						// 	} else {
						// 		sum.redPrev = "0.0";
						// 	}
							
						// 	if(record.C_BLUE !== "0.0" && Number.parseFloat(record.C_BLUE) > Number.parseInt(sum.bluePrev)) {
						// 		if(sum.bluePrev === "0.0") {
						// 			sum.blue += Number.parseInt(record.C_BLUE); 
						// 		}
						// 		sum.bluePrev = record.C_BLUE;
						// 	} else {
						// 		sum.bluePrev = "0.0";
						// 	}
							
						// 	if(record.C_WHITE !== "0.0" && Number.parseFloat(record.C_WHITE) > Number.parseInt(sum.whitePrev)) {
						// 		if(sum.whitePrev === "0.0") {
						// 			sum.white += Number.parseInt(record.C_WHITE);	
						// 		}
						// 		sum.whitePrev = record.C_WHITE;
						// 	} else {
						// 		sum.whitePrev = "0.0";
						// 	}
						// 	return sum;
						// }, { red: 0, blue: 0, white: 0, redPrev:"0.0", bluePrev: "0.0", whitePrev: "0.0"});
						
						// sumValues = {
						// 	red: Number.parseInt(oData.results[0].C_RED),
						// 	blue: Number.parseInt(oData.results[0].C_BLUE),
						// 	white: Number.parseInt(oData.results[0].C_WHITE)
						// };
						
						oJSONModel.setData(lastValues);
						that.getOwnerComponent().setModel(oJSONModel, "sumColorsIoTModel");
						that.setModel(oJSONModel, "sumColorsIoTModel");
						that.getModel("sumColorsIoTModel").refresh();
					},
					error: function(err) {
						sap.m.MessageToast.show("Error reading form IoT demo table.", {
						    duration: 3000,                  // default
						    width: "15em",                   // default
						    my: "center bottom",             // default
						    at: "center bottom",             // default
						    of: window,                      // default
						    offset: "0 0",                   // default
						    collision: "fit fit",            // default
						    onClose: null,                   // default
						    autoClose: true,                 // default
						    animationTimingFunction: "ease", // default
						    animationDuration: 1000,         // default
						    closeOnBrowserNavigation: true   // default
						});
					}
				});
			},

			/* =========================================================== */
			/* event handlers                                              */
			/* =========================================================== */

			/**
			 * Triggered by the table's 'updateFinished' event: after new table
			 * data is available, this handler method updates the table counter.
			 * This should only happen if the update was successful, which is
			 * why this handler is attached to 'updateFinished' and not to the
			 * table's list binding's 'dataReceived' method.
			 * @param {sap.ui.base.Event} oEvent the update finished event
			 * @public
			 */
			onUpdateFinished : function (oEvent) {
				// update the worklist's object counter after the table update
				var sTitle,
					oTable = oEvent.getSource(),
					iTotalItems = oEvent.getParameter("total");
				// only update the counter if the length is final and
				// the table is not empty
				if (iTotalItems && oTable.getBinding("items").isLengthFinal()) {
					sTitle = this.getResourceBundle().getText("worklistTableTitleCount", [iTotalItems]);
				} else {
					sTitle = this.getResourceBundle().getText("worklistTableTitle");
				}
				this.getModel("worklistView").setProperty("/worklistTableTitle", sTitle);
			},

			/**
			 * Event handler when a table item gets pressed
			 * @param {sap.ui.base.Event} oEvent the table selectionChange event
			 * @public
			 */
			onPress : function (oEvent) {
				// The source is the list item that got pressed
				this._showObject(oEvent.getSource());
			},



			/**
			 * Event handler when the share in JAM button has been clicked
			 * @public
			 */
			onShareInJamPress : function () {
				var oViewModel = this.getModel("worklistView"),
					oShareDialog = sap.ui.getCore().createComponent({
						name: "sap.collaboration.components.fiori.sharing.dialog",
						settings: {
							object:{
								id: location.href,
								share: oViewModel.getProperty("/shareOnJamTitle")
							}
						}
					});
				oShareDialog.open();
			},

			onSearch : function (oEvent) {
				if (oEvent.getParameters().refreshButtonPressed) {
					// Search field's 'refresh' button has been pressed.
					// This is visible if you select any master list item.
					// In this case no new search is triggered, we only
					// refresh the list binding.
					this.onRefresh();
				} else {
					var oTableSearchState = [];
					var sQuery = oEvent.getParameter("query");

					if (sQuery && sQuery.length > 0) {
						oTableSearchState = [new Filter("G_DEVICE", FilterOperator.Contains, sQuery)];
					}
					this._applySearch(oTableSearchState);
				}

			},

			/**
			 * Event handler for refresh event. Keeps filter, sort
			 * and group settings and refreshes the list binding.
			 * @public
			 */
			onRefresh : function () {
				var oTable = this.byId("table");
				oTable.getBinding("items").refresh();
			},

			/* =========================================================== */
			/* internal methods                                            */
			/* =========================================================== */

			/**
			 * Shows the selected item on the object page
			 * On phones a additional history entry is created
			 * @param {sap.m.ObjectListItem} oItem selected Item
			 * @private
			 */
			_showObject : function (oItem) {
				this.getRouter().navTo("object", {
					objectId: oItem.getBindingContext().getProperty("G_CREATED")
				});
			},

			/**
			 * Internal helper method to apply both filter and search state together on the list binding
			 * @param {object} oTableSearchState an array of filters for the search
			 * @private
			 */
			_applySearch: function(oTableSearchState) {
				var oTable = this.byId("table"),
					oViewModel = this.getModel("worklistView");
				oTable.getBinding("items").filter(oTableSearchState, "Application");
				// changes the noDataText of the list in case there are no filter results
				if (oTableSearchState.length !== 0) {
					oViewModel.setProperty("/tableNoDataText", this.getResourceBundle().getText("worklistNoDataWithSearchText"));
				}
			}

		});
	}
);