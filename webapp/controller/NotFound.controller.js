sap.ui.define([
		"dynabrains/iot/demo/controller/BaseController"
	], function (BaseController) {
		"use strict";

		return BaseController.extend("dynabrains.iot.demo.controller.NotFound", {

			/**
			 * Navigates to the worklist when the link is pressed
			 * @public
			 */
			onLinkPressed : function () {
				this.getRouter().navTo("worklist");
			}

		});

	}
);