jQuery.sap.require("sap.ui.qunit.qunit-css");
jQuery.sap.require("sap.ui.thirdparty.qunit");
jQuery.sap.require("sap.ui.qunit.qunit-junit");
QUnit.config.autostart = false;

sap.ui.require([
		"sap/ui/test/Opa5",
		"dynabrains/iot/demo/test/integration/pages/Common",
		"sap/ui/test/opaQunit",
		"dynabrains/iot/demo/test/integration/pages/Worklist",
		"dynabrains/iot/demo/test/integration/pages/Object",
		"dynabrains/iot/demo/test/integration/pages/NotFound",
		"dynabrains/iot/demo/test/integration/pages/Browser",
		"dynabrains/iot/demo/test/integration/pages/App"
	], function (Opa5, Common) {
	"use strict";
	Opa5.extendConfig({
		arrangements: new Common(),
		viewNamespace: "dynabrains.iot.demo.view."
	});

	sap.ui.require([
		"dynabrains/iot/demo/test/integration/WorklistJourney",
		"dynabrains/iot/demo/test/integration/ObjectJourney",
		"dynabrains/iot/demo/test/integration/NavigationJourney",
		"dynabrains/iot/demo/test/integration/NotFoundJourney",
		"dynabrains/iot/demo/test/integration/FLPIntegrationJourney"
	], function () {
		QUnit.start();
	});
});